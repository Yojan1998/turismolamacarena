from django.db import models


# Create your models here.
class Transporte(models.Model):
    nombre = models.CharField(max_length=70)
    tipo = models.CharField(max_length=25,
                            choices=(("Aereo", "Aereo"), ("Terrestre", "Terrestre"), ("Fluvial", "Fluvial")))
    horario = models.CharField(max_length=100)
    dias_habiles = models.CharField(max_length=150)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(blank=True, null=True, max_length=20)
    celular = models.PositiveIntegerField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'transporte'
        verbose_name_plural = 'transportes'

    def __str__(self):
        return self.nombre
