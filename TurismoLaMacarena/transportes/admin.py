from django.contrib import admin

# Register your models here.
from transportes.models import Transporte


@admin.register(Transporte)
class TransporteAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'tipo', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo', 'imagen1']

    list_display_links = ['nombre', 'tipo', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo',
                          'imagen1']
