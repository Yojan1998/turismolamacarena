from django.contrib import admin

# Register your models here.
from bancos.models import Banco


@admin.register(Banco)
class BancoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                    'imagen2']

    list_display_links = ['nombre', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                          'imagen2']
