from django.contrib import admin

# Register your models here.
from restaurantes.models import Restaurante


@admin.register(Restaurante)
class RestauranteAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'horario', 'clasificacion', 'direccion', 'telefono', 'celular', 'logo', 'imagen1']

    list_display_links = ['nombre', 'horario', 'clasificacion', 'direccion', 'telefono', 'celular', 'logo', 'imagen1']
