from django.contrib import admin

# Register your models here.
from servicios_de_pagos.models import Servicio_de_pago


@admin.register(Servicio_de_pago)
class Servicio_de_pagoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                    'imagen2']

    list_display_links = ['nombre', 'horario', 'dias_habiles', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                          'imagen2']
