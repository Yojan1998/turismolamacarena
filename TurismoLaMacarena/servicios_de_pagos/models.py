from django.db import models


# Create your models here.
class Servicio_de_pago(models.Model):
    nombre = models.CharField(max_length=70)
    horario = models.CharField(max_length=100)
    dias_habiles = models.CharField(max_length=150)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(blank=True, null=True, max_length=20)
    celular = models.PositiveIntegerField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)
    imagen2 = models.ImageField(blank=True, null=True)
    imagen3 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'servicio_de_pago'
        verbose_name_plural = 'servicios_de_pagos'

    def __str__(self):
        return self.nombre
