from django.apps import AppConfig


class ServiciosDePagosConfig(AppConfig):
    name = 'servicios_de_pagos'
