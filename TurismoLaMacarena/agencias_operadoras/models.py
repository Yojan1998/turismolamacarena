from django.db import models


# Create your models here.
class AgenciaOperadora(models.Model):
    razon_social = models.CharField(max_length=100)
    horario = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(blank=True, null=True, max_length=20)
    celular = models.PositiveIntegerField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)
    imagen2 = models.ImageField(blank=True, null=True)
    imagen3 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'agencia_operadora'
        verbose_name_plural = 'agencias_operadoras'

    def __str__(self):
        return self.razon_social
