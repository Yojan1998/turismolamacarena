from django.contrib import admin

# Register your models here.
from agencias_operadoras.models import AgenciaOperadora


@admin.register(AgenciaOperadora)
class AgenciaOperadoraAdmin(admin.ModelAdmin):
    list_display = ['razon_social', 'horario', 'direccion', 'telefono', 'celular', 'logo', 'imagen1', 'imagen2',
                    'imagen3']
    list_display_links = ['razon_social', 'horario', 'direccion', 'telefono', 'celular', 'logo', 'imagen1', 'imagen2',
                          'imagen3']
