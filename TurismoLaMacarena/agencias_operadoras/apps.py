from django.apps import AppConfig


class AgenciasOperadorasConfig(AppConfig):
    name = 'agencias_operadoras'
