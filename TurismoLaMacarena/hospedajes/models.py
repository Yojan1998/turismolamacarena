from django.db import models


# Create your models here.
class Hospedaje(models.Model):
    nombre = models.CharField(max_length=70)
    horario = models.CharField(max_length=100)
    clasificacion = models.CharField(max_length=1, choices=(("1", "1"), ("2", "2"), ("3", "3"), ("4", "4"), ("5", "5")))
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(blank=True, null=True, max_length=20)
    celular = models.PositiveIntegerField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)
    imagen2 = models.ImageField(blank=True, null=True)
    imagen3 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'hospedaje'
        verbose_name_plural = 'hospedajes'

    def __str__(self):
        return self.nombre
