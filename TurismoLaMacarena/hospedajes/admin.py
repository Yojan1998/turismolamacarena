from django.contrib import admin

# Register your models here.
from hospedajes.models import Hospedaje


@admin.register(Hospedaje)
class HospedajeAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'horario', 'clasificacion', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                    'imagen2', 'imagen3']

    list_display_links = ['nombre', 'horario', 'clasificacion', 'direccion', 'telefono', 'celular', 'logo', 'imagen1',
                          'imagen2', 'imagen3']
