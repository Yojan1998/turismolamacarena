from django.apps import AppConfig


class HospedajesConfig(AppConfig):
    name = 'hospedajes'
