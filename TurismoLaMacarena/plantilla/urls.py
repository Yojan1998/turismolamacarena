from django.urls import path

from plantilla.views import PlantillaView

urlpatterns = [
    path('', PlantillaView.as_view(), name='index'),
]
