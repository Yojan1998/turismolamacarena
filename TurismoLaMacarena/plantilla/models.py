from django.db import models


# Create your models here.

class ConfiguracionIndex(models.Model):
    nombre_de_la_pagina = models.CharField(max_length=20, blank=True, null=True)
    texto1 = models.CharField(max_length=20, blank=True, null=True)
    texto2 = models.CharField(max_length=20, blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    favicon = models.ImageField(blank=True, null=True)
    banner1 = models.ImageField(blank=True, null=True)
    banner2 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'configuracion_Index'
        verbose_name_plural = 'configuraciones_Index'

    def __str__(self):
        return self.nombre_de_la_pagina
