from django.contrib import admin


# Register your models here.
from plantilla.models import ConfiguracionIndex


@admin.register(ConfiguracionIndex)
class ConfiguracionIndexAdmin(admin.ModelAdmin):
    list_display = ['texto1', 'texto2','nombre_de_la_pagina', 'logo', 'favicon', 'banner1', 'banner2']
    list_display_links = ['texto1', 'texto2', 'nombre_de_la_pagina', 'logo', 'favicon', 'banner1', 'banner2']
