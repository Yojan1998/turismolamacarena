from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from plantilla.models import ConfiguracionIndex


class PlantillaView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['configuraciones'] = ConfiguracionIndex.objects.all()
        return data
