from django.contrib import admin

# Register your models here.
from eventos.models import Evento


@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'anfitrion', 'fecha', 'lugar', 'durancion', 'precio', 'telefono', 'celular', 'logo',
                    'imagen1', 'imagen2', 'imagen3']
    list_display_links = ['nombre', 'anfitrion', 'fecha', 'lugar', 'durancion', 'precio', 'telefono', 'celular', 'logo',
                          'imagen1', 'imagen2', 'imagen3']
