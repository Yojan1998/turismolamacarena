from django.db import models


# Create your models here.
class Evento(models.Model):
    nombre = models.CharField(max_length=70)
    anfitrion = models.CharField(max_length=100)
    fecha = models.DateField()
    lugar = models.CharField(max_length=100)
    durancion = models.IntegerField()
    precio = models.CharField(max_length=100)
    telefono = models.CharField(blank=True, null=True, max_length=100)
    celular = models.PositiveIntegerField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    imagen1 = models.ImageField(blank=True, null=True)
    imagen2 = models.ImageField(blank=True, null=True)
    imagen3 = models.ImageField(blank=True, null=True)

    class Meta:
        verbose_name = 'evento'
        verbose_name_plural = 'eventos'

    def __str__(self):
        return self.nombre
